import 'dotenv/config';
import fetch from 'node-fetch';
import Handlebars from 'handlebars';
import fs from 'fs';
import { readFileSync, writeFileSync } from 'fs';
import { marked } from 'marked';
import path from 'path';

// Helper function to create a directory if it doesn't exist
function ensureDirectoryExists(directory) {
    if (!fs.existsSync(directory)) {
        fs.mkdirSync(directory, { recursive: true });
    }
}

// Helper function to read and compile a Handlebars template from a file
function compileTemplate(filename) {
    const templateString = readFileSync(filename, 'utf-8');
    return Handlebars.compile(templateString);
}

// Helper function to convert Markdown to HTML
function markdownToHtml(markdown) {
    return markdown ? marked(markdown) : '';
}

async function fetchIssuesForProject(projectId, token) {
    const url = `https://gitlab.com/api/v4/projects/${projectId}/issues`;
    const response = await fetch(url, {
        headers: { 'Authorization': `Bearer ${token}` }
    });
    if (!response.ok) {
        throw new Error(`Error fetching issues for project ${projectId}: ${response.statusText}`);
    }
    return response.json();
}

async function fetchAllIssues() {
    const projectIds = process.env.PROJECT_IDS.split(',');
    const projectTitles = process.env.PROJECT_TITLES.split(',');
    const projectLinks = process.env.PROJECT_LINKS.split(',');
    const token = process.env.YOUR_ACCESS_TOKEN;
    let allIssues = [];

    for (let i = 0; i < projectIds.length; i++) {
        const projectId = projectIds[i];
        const projectTitle = projectTitles[i] || `Project ${i + 1}`;
        const projectLink = projectLinks[i] || `https://gitlab.com/${projectId}`;

        try {
            // Fetch issues for the current project
            const issues = await fetchIssuesForProject(projectId, token);

            for (const issue of issues) {
                try {
                    // Fetch notes for each issue
                    const notes = await fetchIssueNotes(projectId, issue.iid, token);
                    // Sort notes by created_at, latest first
                    issue.notes = notes.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
                    if(issue.notes[0])
                        issue.lastNote = issue.notes[0];

                } catch (error) {
                    console.error(`Failed to fetch notes for issue ${issue.iid}:`, error.message);
                    issue.notes = []; // Ensure issue has an empty notes array if fetching notes fails
                }

                // Assign the project title to each issue
                issue.projectName = projectTitle;
                issue.projectLink = projectLink;
                issue.projectIssueLink = `${projectLink}/issues/${issue.iid}`;
            }

            // Concatenate the issues from the current project with the main issues array
            allIssues = allIssues.concat(issues);

        } catch (error) {
            console.error(`Failed to fetch issues for project ${projectId}:`, error.message);
        }
    }

    // After fetching all issues, sort them by the 'updated_at' field
    allIssues.sort((a, b) => new Date(b.updated_at) - new Date(a.updated_at));

    return allIssues;
}


async function generateHTML(allIssues, token) {
    const pageTitle = process.env.PAGE_TITLE;
    const listTemplate = compileTemplate('issuesTemplate.hbs');
    const notesTemplate = compileTemplate('issue-detail-template.hbs');

    // Generate HTML files for each issue
    for (const issue of allIssues) {
        // Convert Markdown to HTML for description and notes
        issue.descriptionHtml = markdownToHtml(issue.description);
        for (const note of issue.notes) {
            note.bodyHtml = markdownToHtml(note.body);
            note.created = new Date(note.created_at || note.created_at).toLocaleString() // Format date
        }

        // Get the project title and issue ID
        const projectTitle = issue.projectName.toLowerCase().replace(/\s+/g, '_');
        const issueId = issue.iid;

        // Create a directory for the project if it doesn't exist
        const projectDirectory = path.join('output', projectTitle);
        ensureDirectoryExists(projectDirectory);

        // Define the file paths
        const issueFilename = `${projectTitle}_${issueId}.html`;
        const issueFilePath = path.join(projectDirectory, issueFilename);

        // Update the detailsLink with the correct relative path
        issue.detailsLink = path.join(projectTitle, issueFilename);

        issue.created = new Date(issue.created_at || issue.created_at).toLocaleString() // Format date

        // Save the HTML file in the project folder
        writeFileSync(issueFilePath, notesTemplate(issue));
    }

    const data = {
        pageTitle: process.env.PAGE_TITLE,
        issues: allIssues.map(issue => ({
            id: issue.iid,
            projectIssueLink: issue.projectIssueLink,
            detailsLink: issue.detailsLink,
            projectName: issue.projectName,
            iconClass: issue.state === 'opened' ? process.env.OPEN_ISSUE_ICON : process.env.CLOSED_ISSUE_ICON,
            rowClass: issue.state === 'opened' ? 'open' : 'closed',
            title: issue.title,
            lastNote: issue?.lastNote?.body,
            updated: new Date(issue.updated_at || issue.created_at).toLocaleString() // Format date
        }))
    };
    // Generate the main issue list HTML
    const listHtml = listTemplate(data);
    writeFileSync('output/issues.html', listHtml);
}

async function fetchIssueNotes(projectId, issueIid, token) {
    const url = `https://gitlab.com/api/v4/projects/${projectId}/issues/${issueIid}/notes`;
    const response = await fetch(url, {
        headers: { 'Authorization': `Bearer ${token}` }
    });
    if (!response.ok) {
        throw new Error(`Error fetching notes for issue ${issueIid}: ${response.statusText}`);
    }
    return response.json();
}

// Assuming you have an updated fetchAllIssues function that fetches issues and their notes
async function main() {
    const token = process.env.YOUR_ACCESS_TOKEN;
    // Assume fetchAllIssues now also fetches notes for each issue
    const allIssues = await fetchAllIssues(token);
    await generateHTML(allIssues, token);
}

main();
